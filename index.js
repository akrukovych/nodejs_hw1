const http = require("http");
const fs = require("fs");
const url = require("url");
const path = require("path");
const { promisify } = require("util");

const readFileAsync = promisify(fs.readFile);
const writeFileAsync = promisify(fs.writeFile);
const accessAsync = promisify(fs.access);
const mkdirAsync = promisify(fs.mkdir);

const PORT = 8080;
const CODING = "utf8";
const POST = "POST";
const GET = "GET";
const FILE_ENDPOINT = "/file";
const LOGS_ENDPOINT = "/logs";
const DIR_NAME = "files";
const LOGS = "logs.json";

const readLogs = () => {
  return readFileAsync(LOGS, CODING);
};

const prettyStringify = (obj) => JSON.stringify(obj, null, 2);

const logMessage = (message) => {
  readLogs()
    .then((logsObject) => {
      logsObject = JSON.parse(logsObject);
      const logMessage = { message, time: new Date().getTime() };
      logsObject.logs.push(logMessage);
      const stringifyLogs = JSON.stringify(logsObject, null, 2);
      writeFileAsync(LOGS, stringifyLogs, CODING).catch((e) => console.log(e));
    })
    .catch((e) => console.log(e));
};

const generateFilePath = (filename) => path.join(__dirname, DIR_NAME, filename);

const returnError = (res, status, message) => {
  res.statusCode = status;
  logMessage(message);
  return res.end(prettyStringify({ message }));
};

const respondWithBadRequestError = (res) => {
  returnError(res, 400, "Bad request error");
};

const respondWithInternalServerError = (res) => {
  returnError(res, 500, "Internal server error");
};

const postFileHandler = (res, filename, content) => {
  if (!filename || !content) return respondWithBadRequestError(res);

  writeFileAsync(generateFilePath(filename), content, CODING)
    .then(() => {
      res.statusCode = 200;
      const message = `New file with name '${filename}' saved`;
      logMessage(message);
      return res.end(prettyStringify({ message }));
    })
    .catch(() => {
      respondWithInternalServerError(res);
    });
};

const getFileHandler = (res, pathname) => {
  const fileName = pathname.replace(`${FILE_ENDPOINT}/`, "");
  readFileAsync(generateFilePath(fileName), CODING)
    .then((content) => {
      res.statusCode = 200;
      const message = `${fileName} has been read`;
      logMessage(message);
      return res.end(content);
    })
    .catch(() => {
      respondWithBadRequestError(res);
    });
};

const getLogsHandler = (res, from, to) => {
  readLogs().then((logsObject) => {
    logsObject = JSON.parse(logsObject);
    const { logs } = logsObject;
    if (from && to) {
      logsObject.logs = logs.filter(
        (log) => log.time >= +from && log.time <= +to
      );
    }
    res.statusCode = 200;
    return res.end(prettyStringify(logsObject));
  });
};

module.exports = () => {
  accessAsync(LOGS)
    .catch(() => writeFileAsync(LOGS, prettyStringify({ logs: [] }), CODING))
    .then(() => console.log(`${LOGS} is created`))
    .catch((e) => console.log(e));

  mkdirAsync(DIR_NAME)
    .then(() => console.log(`${DIR_NAME} directory created successfully!`))
    .catch((e) => console.log(e));

  http
    .createServer((req, res) => {
      res.setHeader("Content-Type", "application/json");

      const {
        pathname,
        query: { filename, content, from, to },
      } = url.parse(req.url, true);

      if (pathname.startsWith(FILE_ENDPOINT)) {
        req.method === POST
          ? postFileHandler(res, filename, content)
          : getFileHandler(res, pathname);
      } else if (pathname.startsWith(LOGS_ENDPOINT)) {
        req.method === GET
          ? getLogsHandler(res, from, to)
          : respondWithBadRequestError(res);
        getLogsHandler(res, from, to);
      } else {
        respondWithBadRequestError(res);
      }
    })
    .listen(process.env.PORT || PORT);
};
